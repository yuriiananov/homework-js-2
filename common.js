"use strict";

/*Завдання
Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера – alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
1. Отримати за допомогою модального вікна браузера дані користувача: ім’я та вік.
2. Якщо вік менше 18 років – показати на екрані повідомлення: You are not allowed to visit this website.
3. Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. 4.4.4. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім’я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
5. Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім’я користувача.
6. Обов’язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
7. Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім’я, або при введенні віку вказав не число – запитати ім’я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).*/

let name = prompt("Введіть своє ім'я:", "");
let age = +prompt("Введіть свій вік:", "");

let getUserData = {
    name: name,
    age: age,
};

while (!getUserData.name || isNaN(getUserData.age)) {
    getUserData.name = prompt("Bведіть правильно своє ім'я:", !getUserData.name ? "" : getUserData.name);
    getUserData.age = +prompt("Введіть правильно свій вік:", isNaN(getUserData.age) ? "" : getUserData.age);
}

if (getUserData.age < 18) {
  alert("You are not allowed to visit this website.");
} else if (getUserData.age >= 18 && getUserData.age <= 22) {
  let confirmResult = confirm("Are you sure you want to continue?");
  if (confirmResult) {
    alert("Welcome, " + getUserData.name + "!");
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert("Welcome, " + getUserData.name + "!");
}

